// import Image from "next/image";
// import styles from "./page.module.css";

// export default function Home() {
//   return (
//     <main className={styles.main}>
//       <h1>hola mundo</h1>
//     </main>
//   );
// }
"use client"
import MyCard from './components/MyCard';
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Search } from '@mui/icons-material';



interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
}

const drawerWidth = 240;
const navItems = ['Home','Category','Trending News', 'Recent News','Club Ranking', 'Sports Article'];

export default function Home(props: Props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography variant="h6" sx={{ my: 2 }}>
        SPORT NEWS
      </Typography>
      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={item} disablePadding>
            <ListItemButton sx={{ textAlign: 'center' }}>
              <ListItemText primary={item} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar component="nav">
        <Toolbar  sx={{display:'flex',justifyContent:'space-between', backgroundColor:'#fff'}}>
          <Typography
            variant="h6"
            component="div"
            sx={{color:'#000',  }}
          >
            Sport News
          </Typography>
          <Box >
            {navItems.map((item) => (
              <Button key={item} sx={{ color: 'rgba(38, 38, 38, 0.6)' }}>
                {item}
              </Button>
            ))}
          </Box>
          <Box>
          <Button variant="outlined" startIcon={<Search />} sx={{color:'#fff',backgroundColor:'rgba(184, 194, 206, 1)',border:'none'}}>
            Search
          </Button>
          </Box>
        </Toolbar>
      </AppBar>
      <nav>
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          // sx={{
          //   display: { xs: 'block', sm: 'none' },
          //   '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          // }}
        >
          {drawer}
        </Drawer>
      </nav>
      <Box component="main" >
        <Box sx={{ display:'flex',flexDirection:'row',marginTop:'70px', width:'100%',height:'100%',position:'relative'}}>
          <Box sx={{backgroundImage:'./public/images/Jugador.svg',height:'100%',width:'970px',display:'block'}}>


          </Box>
          <Box sx={{width:'300px',display:'flex',flexDirection:'column',justifyContent:'space-between',alignItems:'start'}}>
            <Button variant="outlined"  sx={{color:'rgba(184, 194, 206, 1)',backgroundColor:'rgba(225, 232, 240, 1)',border:'none'}}>
              Today
            </Button>
            <MyCard></MyCard>
            <MyCard></MyCard>
          </Box>
          
        </Box>
         
      </Box>
    </Box>
  );
}


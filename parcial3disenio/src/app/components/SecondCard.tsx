import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { Box, Button } from '@mui/material';

interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function SecondCard() {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ maxWidth:400, height:'90%', margin:'0px 20px' }}>
      <Box>
      <Button variant="outlined" sx={{position:'absolute',zIndex:'1',textTransform: 'none',
        color:'rgba(235, 238, 243, 1)',border:'rgba(235, 238, 243, 1) solid 1px',marginLeft:'200px',marginTop:'15px'}}>Basquetball</Button>
      <CardMedia
        component="img"
        height="194"
        image="/images/BasquetBall.svg"
        alt="baner"
        sx={{display:'block',position:'static'
        }}
      />
      
      </Box>
      
      

      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: red[500], }} aria-label="recipe">
            <CardMedia
                sx={{display:'block',position:'relative'}}
                component="img"
                height="100%"
                image="/images/OneAvatar.svg"
                alt="avatar"
            />
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title="Jake Will"
        // subheader="September 14, 2016"
      />
      <Typography  variant='body2' color="rgba(105, 104, 104, 1)" sx={{fontWeight: 'bold',marginLeft:'15px'}}>
        04 june 2023
      </Typography>
      <CardContent>
      
        <Typography  variant='subtitle1' color="#000" sx={{fontWeight: 'bold'}}>
        5 Exercises Basketball Players Should <br />
        Be Using To Develop Strength
        </Typography>
        <Typography variant='body2' color="rgba(105, 104, 104, 1)">
        This article was written by Jake Willhoite 
        from <br /> Healthlisted.com Strength in basketball isn’t all <br /> about 
        a massive body mass or ripped muscles.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      
    </Card>
  );
}

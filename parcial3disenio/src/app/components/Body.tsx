"use client"
import { Box, Typography } from "@mui/material";
import SecondCard from "./SecondCard";
import * as React from 'react';
import { styled } from '@mui/material/styles';

export default function Body() {
    return (
        <Box sx={{marginTop:'80px'}}>
            <Typography  variant='h4' color="#000" sx={{fontWeight: 'bold',paddingLeft:'145px'}}>
                Sports Article
            </Typography>
            <Box className="SportArticle">
            <SecondCard/>
            <SecondCard/>
            <SecondCard/>
            </Box>
        </Box>
      
    );
  }
import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, CardActions } from '@mui/material';

export default function MyCard() {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardActionArea>
        <CardContent sx={{position:'absolute',marginTop:'120px'}}>
          <Typography gutterBottom variant="body2" component="div">
          Race98 - 03 June 2023
          </Typography>
          <Typography gutterBottom variant="h6" component="div">
          Ethiopian runners took the top four spots.
          </Typography>
        
        </CardContent>
        <CardMedia
          component="img"
          height="140"
          image="/images/womanrun.svg"
          alt="green iguana"
          sx={{display:'block',position:'static',height:'100%', backgroundImage: 'linear-gradient(0deg, rgba(245,238,238,1) 35%, rgba(57,57,57,1) 85%)'}}
        />
        
      </CardActionArea>
    </Card>
  );
}
